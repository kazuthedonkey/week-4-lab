﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using UnityEngine;

public class PrintAndHide : MonoBehaviour
{
    int i = 0;
    public Renderer rend;
    int counter;

    // Start is called before the first frame update
    void Start()
    {
        counter = Random.Range(200, 250);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(gameObject.name + ":" + i);

        if (gameObject.CompareTag("Red") && i == 100)
            gameObject.SetActive(false);

        if (gameObject.CompareTag("Blue") && i == counter)
            rend.enabled = false;

        ++i;
    }
}
